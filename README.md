# **SSH keys aggragator for Managed Cloud**

This stack resolves problem described at

https://nordcloud.atlassian.net/wiki/spaces/MC/pages/998047843/Managed+Cloud+SSH+Key+File+Tool

## **STACK RESULTS**

** Cloudformation stack will provision**

  - Source S3 bucket (to store public ssh keys)
  
  - Target S3 bucket (to upload authorized_keys and zipped public ssh keys from Source S3 bucket)
  
  - Lambda function that downloads public ssh keys from source S3 bucket, performs transformations and uploads files to target S3 bucket
  
  - IAM Role for lambda function that allows read access to source and write access to target bucket
  
  - Bucket policies for both source&target buckets from NC IPs via VPN
  
  - SNS topic for sending error messages and when files are ready for download from target bucket
  

## **DEPLOYMENT INSTRUCTION**

  **1. Deploy CFN template via Cloudformation**

  either via console or via aws cli with
  ```aws cloudformation deploy --template-file ssh_pub_keys_aggregator.yml --stack-name ${CFN_STACK_NAME} --capabilities CAPABILITY_IAM --profile ${AWS_USER_PROFILE_FOR_CLI}```
  
  you can use two parameters
  
  - FileExtensionToInclude to include other files than .pub - default .pub
  
  - FileExtensionToInclude to include other IPs to allow access to S3 buckets - default 52.212.230.104/32,52.214.217.132/32
    

  **2. Add Lambda Notification to Source S3 bucket**

  It is not possible to do that via CFN as there would be circular dependencies.
  First, list exported values from CFN stack from step above
  ```aws --profile ${AWS_USER_PROFILE_FOR_CLI} cloudformation list-exports```
  You should expect output similar to below

    {
        "Exports": [
            {
                "ExportingStackId": "arn:aws:cloudformation:eu-west-1:1234567890:stack/cfnstack/09500ad0-ccce-11e8-9450-50a686343cd2",
                "Name": "cfnstack:SSHaggregatorLambdaFunctionArn",
                "Value": "arn:aws:lambda:eu-west-1:1234567890:function:cfnstack-SSHaggregatorLambdaFunction-1AR0IZQ9MHNTE"
            },
            {
                "ExportingStackId": "arn:aws:cloudformation:eu-west-1:1234567890:stack/cfnstack/09500ad0-ccce-11e8-9450-50a686343cd2",
                "Name": "cfnstack:SSHaggregatorSourceBucketName",
                "Value": "cfnstack-sourcebucket-12unvlqpcp7ep"
            }
        ]
    }

  Note Source Bucket Name (```sshkeyaggregator-sourcebucket-txhftyxvije5```) and Lambda ARN (```arn:aws:lambda:eu-west-1:1234567890:function:SSHkeyAggregator-SSHaggregatorLambdaFunction-1CKPWXWK9LSOU```) values - use them to update JSON structure below with LAMBDA_ARN

    {
        "CloudFunctionConfiguration": {
            "CloudFunction": "${LAMBDA_ARN}",
            "Events": [
                "s3:ObjectCreated:*",
                "s3:ObjectRemoved:*"
            ]
        }
    }

  save file as S3notifications.json and launch command with proper SOURCE_BUCKET_NAME variable

  ```aws --profile ${AWS_USER_PROFILE_FOR_CLI} s3api put-bucket-notification --bucket ${SOURCE_BUCKET_NAME} --notification-configuration file://S3notifications.json```

  **3. Subscribe to created SNS topic (available in created Resource tab in cloudformation)**

  **4. Upload .pub files into Source S3 bucket**

  Please note, each uploaded/deleted file generates SNS notification and output file creation.